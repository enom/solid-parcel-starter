import { render } from 'solid-js/web'

function App () {
  return (
    <p>Solid Parcel Starter</p>
  )
}

render(() => <App />, document.getElementById('root'))
