# Solid Parcel Starter

Minimal [Solid](https://github.com/ryansolid/solid) starter for [Parcel v2](https://v2.parceljs.org/).

## Installation

This starter assumes you don't have the Parcel bundler installed globally.

You can quickly clone and discard this repository's Git history by using `npx` and the `tiged` package:

```sh
npx tiged gitlab:enom/solid-parcel-starter
```

Install the necessary packages using NPM or Yarn:

```sh
npm install
```
### Tailwind CSS

If you're interested in working with [Tailwind CSS](https://tailwindcss.com/) then you can use the `tailwindcss` branch:

```sh
npx tiged gitlab:enom/solid-parcel-starter#tailwindcss
```

### Parcel v1

To use the legacy version of Parcel, please see the `v1` branch.

## Usage

The included `package.json` has two scripts:

```sh
npm run dev
# parcel serve src/index.html
```

```sh
npm run build
# parcel build src/index.html
```

## Dependency Hell

Parcel makes dependency hell incredibly easy to resolve: https://gitlab.com/dashdashalias/parcel.
